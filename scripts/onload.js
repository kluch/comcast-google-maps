//   This Javascript was originally provided by Mike Williams
//   Blackpool Community Church Javascript Team
//   http://www.commchurch.freeserve.co.uk/
//   http://econym.org.uk/gmap/
//   Modified by Joe Kluchinski

var map;
var directionsService;
var directionsDisplay;
var endMarker;
var polyline;
var canvas;
var mapWidth;
var menuWidth;
var addresses = ['605 Walden Drive, West Chester, PA 19380, USA',
                 '516 Trinity Drive, West Chester, PA 19382, USA',
                 '138 Spruce Alley, West Chester, PA 19382, USA',
                 '798 Autopark Boulevard, West Chester, PA 19382, USA',
                 '204 East Virginia Avenue, West Chester, PA 19380, USA'];
var docHeight;
var poly2; // TODO: Delete Me
var supportsCanvas; // Checks if browser supports <canvas>
var rImg = new Image();
rImg.src='rxcomcast_small.jpg'; rImg.width = 100; rImg.height = 46;
var lImg = new Image();
lImg.src='lxcomcast_small.jpg'; lImg.width = 100; lImg.height = 46;
var img = new Image();
img.src = 'glympse.png'; img.width = 50; img.height = 142;

// Runs onload of <body>
function initialize() {
  "use strict";

  //this is for setting map height
  //TODO: remove this once directions is no longer done with form
  // docHeight = $('body').height();
  // document.getElementById('map_canvas').style.height = (docHeight-24)+'px';
  // document.getElementById('menu').style.top = (10+24)+'px';

  // mapWidth = document.getElementById("map_canvas").offsetWidth;
  // menuWidth = document.getElementById("menu").offsetWidth;

  // This checks for the testcanvas (which should exists)
  if (document.getElementById('testcanvas').getContext) {
    supportsCanvas = true;
  }
  else {
    supportsCanvas = false;
  }
  
  // Map options with UI disabled.
  var myOptions = {
    zoom: 3,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  };

  // Options used by Directions Display
  var rendererOptions = {
    map: map,
    suppressMarkers: true,
    suppressPolylines: true
  };

  // Center on the US and use the above map options
  makeMap(document.getElementById("map_canvas"), myOptions, 'United States', rendererOptions);


  // TODO: Delete this once directions are pulled from database and button is no longer used
  // These lines get created here but get overwritten on button press..
  // Only reason to leave them in is so setting them to null in start() doesn't throw an error
  polyline = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3
  });
  poly2 = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3,
    strokeOpacity: 0.5
  });

  //This happens on click
  start2();
}

/**
 * ------------------------------ Start The Loop ------------------------------
 */
function start2() {
  // resets values
  // this shouldn't be needed once I get rid of the start button
  if (timerHandle) { clearTimeout(timerHandle); }
  if (updateETATimeout) { clearInterval(updateETATimeout); }
  if (updatedTimeout) { clearInterval(updatedTimeout); }
  if (marker) { marker.setMap(null);}
  if (endMarker) { endMarker.setMap(null);}
  distance = 0;
  polyline.setMap(null);
  poly2.setMap(null);
  directionsDisplay.setMap(null);

  //randomize an account ID
  document.getElementById('accnt_num').innerHTML = Math.floor(Math.random()*(9999999999-1111111111)+1111111111);

  // randomize an appt window
  var win_hr1 = Math.floor(Math.random()*(12-1)+1);
  var win_hr2;
  var win_min = Math.floor(Math.random()*(3))*15;

  if(win_min<10){
    win_min = '0' + win_min;
  }
  if(win_hr1<=6){
    win_hr2 = win_hr1 + 2;
    
    document.getElementById('win_hr1').innerHTML = win_hr1;
    document.getElementById('win_hr2').innerHTML = win_hr2;
    document.getElementById('win_min1').innerHTML = win_min + ' PM';
    document.getElementById('win_min2').innerHTML = win_min + ' PM';
  }
  else{
    if(win_hr1<=10){
      win_hr2 = (win_hr1 + 2);
      document.getElementById('win_hr1').innerHTML = win_hr1;
      document.getElementById('win_hr2').innerHTML = win_hr2;
      document.getElementById('win_min1').innerHTML = win_min + ' AM';
      document.getElementById('win_min2').innerHTML = win_min + ' AM';
    }

    else{
      win_hr2 = (win_hr1 + 2) - 12;
      document.getElementById('win_hr1').innerHTML = win_hr1;
      document.getElementById('win_hr2').innerHTML = win_hr2;
      document.getElementById('win_min1').innerHTML = win_min + ' AM';
      document.getElementById('win_min2').innerHTML = win_min + ' PM';
    }
  }
  

  polyline = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3,
    visible: false
  });
  poly2 = new google.maps.Polyline({
    path: [],
    strokeColor: '#000055',
    strokeWeight: 5,
    strokeOpacity: 0.5
  });

  // Create a renderer for directions and bind it to the map.
  var rendererOptions = {
    map: map,
    suppressMarkers: true,
    suppressPolylines: true
  };
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

  var startpoint;
  var endpoint;

  console.log("end address: " + typeof endLocation.address);

  // If endpoint exists make startpoint and get new endpoint
  if(typeof endLocation.address === 'string'){
    startpoint = endLocation.address;
    endpoint = addresses[Math.floor(Math.random()*5)];
  }
  else{
    startpoint = addresses[Math.floor(Math.random()*5)];
    endpoint = addresses[Math.floor(Math.random()*5)];
  }
  while(startpoint === endpoint){
    endpoint = addresses[Math.floor(Math.random()*5)];
  }
  console.log("startpoint: " + startpoint);
  console.log("endpoint: " + endpoint);


  var travelMode = google.maps.DirectionsTravelMode.DRIVING;

  var request = {
    origin: startpoint,
    destination: endpoint,
    travelMode: travelMode
  };

  getDirections(request);
  // This should check if there is new live data, if there is
  // build a request with the latlng otherwise dec the ETA
  updateETATimeout = setInterval(updateETA, 60000);
  updatedTimeout = setInterval(updated, 1000);
}
/**
 * ------------------------------ Start The Loop ------------------------------
 */


// This runs when the window is resized
// Will check that the directionsPanel still fits and will change the
// offset value for the map center.
var windowTimeOut = null;
window.onresize = function(){
  if(windowTimeOut !== null){
    clearTimeout(windowTimeOut);
  }
  windowTimeOut = setTimeout(windowResized, 200);
};
function windowResized(){
  // This is where the code lives getchu one!
  // mapWidth = document.getElementById("map_canvas").offsetWidth;
  // menuWidth = document.getElementById("menu").offsetWidth;
  // docHeight = $('body').height();
  // document.getElementById('map_canvas').style.height = (docHeight-24)+'px';
}

// function offsetCenter(latlng,offsetx,offsety) {

// // latlng is the apparent centre-point
// // offsetx is the distance you want that point to move to the right, in pixels
// // offsety is the distance you want that point to move upwards, in pixels
// // offset can be negative
// // offsetx and offsety are both optional

// var scale = Math.pow(2, map.getZoom());
// var nw = new google.maps.LatLng(
//     map.getBounds().getNorthEast().lat(),
//     map.getBounds().getSouthWest().lng()
// );

// var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
// var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0);

// var worldCoordinateNewCenter = new google.maps.Point(
//     worldCoordinateCenter.x - pixelOffset.x,
//     worldCoordinateCenter.y + pixelOffset.y
// );

// var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

// map.setCenter(newCenter);

// }

// function goTo(latlng, firstrun){
//   "use strict";
//   var scale = Math.pow(2, map.getZoom());
//   var x = mapWidth / 2;
//   var offsetx = ((mapWidth - menuWidth) / 2) + menuWidth - x;
  
//   var mapsCenter = map.getProjection().fromLatLngToPoint(latlng);
//   var pixelOffset = new google.maps.Point((offsetx/scale) || 0, 0);

//   // Changes latlng value to the new center
//   var mapsNewCenter = new google.maps.Point(mapsCenter.x - pixelOffset.x, mapsCenter.y);
//   latlng = map.getProjection().fromPointToLatLng(mapsNewCenter);
//   if(firstrun){
//     map.setCenter(latlng);
//   }
//   else{
//     map.panTo(latlng);
//   }
// }




