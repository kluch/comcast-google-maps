/**
 * ---------------------------------ETA Timer----------------------------------
 * This uses the directionsService to get the ETA time and updates the display
 */
var updateETATimeout;
function updateETA() {
  var startTimeTest = new Date().getTime();
  // logic here
  update_time = 0;
  var directions;
  var request = {};
  // This is where the program would try to get real data
  // if real data is not availible get data from animation
  if(true){
    request = {
      origin: polyline.GetPointAtDistance(distance),
      destination: endLocation.latlng,
      travelMode: google.maps.TravelMode.DRIVING
    };
  }
  var directionsRoute = directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directions = response;
      console.log(directions.routes[0].legs[0].duration.text);
      console.log("DO YOU SEE THIS?");
      var txtTime = "";
      var time = 0;
      var hours = 0;
      var minutes = 0;
      var totDistance = 0;
      var legs = directions.routes[0].legs;
      var disRemaining = eol-distance;
      for (var i = 0; i < legs.length; i++) {
        time += legs[i].duration.value;
        totDistance += legs[i].distance.value;
      }
      // currentETA = time;
      // hours = Math.floor(time/3600);
      // time = time - (hours*3600);
      // minutes = Math.floor(time/60);
      // time = time - (minutes*60);
      // if(hours>0){
      //   txtTime = hours+" hours";
      //   if(minutes>0){
      //     txtTime = txtTime+", "+minutes+" minutes";
      //     if(time>0){
      //       txtTime = txtTime+", "+time+" seconds";
      //     }
      //   }
      // }
      // else if(minutes>0){
      //   txtTime = minutes+" minutes";
      //   if(time>0){
      //     txtTime = txtTime+", "+time+" seconds";
      //   }
      // }
      if(time>60){
        txtTime = legs[0].duration.text;
      }
      else if(time>0){
        txtTime = "Your tech should arrive within the next few minutes."; //time+" seconds";
        clearInterval(updateETATimeout);
      }
      else{
        txtTime = "Oops.. negative time?";
      }
      var endTimeTest = new Date().getTime() - startTimeTest;
      document.getElementById('distance').innerHTML = (Math.round10(0.000621371*totDistance,-1))+" miles";
      document.getElementById('time').innerHTML = "ETA: "+txtTime;
      document.getElementById('update_time').innerHTML = update_time + ' sec';

      // This updates the step var in animate to reflect realtime
      step = (disRemaining/time)*(tick/1000);
      console.log("new step distance: "+step);

      // recurse
      // updateETA();
    }
  });
}
/**
 * ---------------------------------ETA Timer----------------------------------
 */

/**
 * -------------------------------Car Animation--------------------------------
 */
function animate2(d) {
  var findDistance = google.maps.geometry.spherical;
  distance = d;
    if (distance>eol) {
      clearInterval(updateETATimeout);
      start2();
      return;
    }
    var p = polyline.GetPointAtDistance(distance);
    if (k++>=15) {
      // map.panTo(p);
      // We want to go between p and endpoint
      var middle = new google.maps.LatLng( ((p.lat() + endLocation.latlng.lat()) / 2), ((p.lng() + endLocation.latlng.lng()) / 2) );
      goTo(middle);
      k=0;
    }
    //console.log('p: '+marker.getProjection().fromLatLngToDivPixel(p));
    marker.setPoint(p);

    // Replace poly2.getPath().getAt(0) with var p and
    // check if we should remove poly2.getPath().getAt(0)
    if((findDistance.computeDistanceBetween(polyline.GetPointAtDistance(distance),poly2.getPath().getAt(1)) < 10) && (poly2.getPath().length > 2)){
      poly2.getPath().insertAt(0,p);
      poly2.getPath().removeAt(1);
      poly2.getPath().removeAt(1);
    }

    //document.getElementById("distance").innerHTML =  "Miles: "+(distance/1609.344).toFixed(2)+speed;
    /* if (stepnum+1 < dirn.getRoute(0).getNumSteps()) {
    var legs = dirn.routes[0];
    var steps = legs[0].steps;
    //if (stepnum+1 < steps.length) {
      if (dirn.getRoute(0).getStep(stepnum).getPolylineIndex() < poly.GetIndexAtDistance(d)) {
        stepnum++;
        var steptext = dirn.getRoute(0).getStep(stepnum).getDescriptionHtml();
        document.getElementById("step").innerHTML = "<b>Next:<\/b> "+steptext;
        var stepdist = dirn.getRoute(0).getStep(stepnum-1).getDistance().meters;
        var steptime = dirn.getRoute(0).getStep(stepnum-1).getDuration().seconds;
        var stepspeed = ((stepdist/steptime) * 2.24).toFixed(0);
        step = stepspeed/2.5;
        speed = "<br />Current speed: " + stepspeed +" mph";
      }
    } else {
      if (dirn.getRoute(0).getStep(stepnum).getPolylineIndex() < poly.GetIndexAtDistance(d)) {
        document.getElementById("step").innerHTML = "<b>Next: Arrive at your destination<\/b>";
      }
    }*/
    if (supportsCanvas) {
      //console.log("this is the eol: "+eol+' and distance: '+distance);
      if (distance+step<eol) {
        // lastVertex = polyline.GetPointAtDistance(d);
        // if (lastVertex == polyline.getPath().length) {
        //   lastVertex -= 1;
        // }
        // while (polyline.getPath(lastVertex-1).equals(polyline.getPath(lastVertex))) {
        //   lastVertex-=1;
        // }
        // angle = bearing(polyline.getPath(lastVertex-1),polyline.getPath(lastVertex) );
        plotcar2(distance);
      }
    }

    // step is how far along the polyline I want to jump
    // tick is how fast I want to jump
    window.setTimeout(function(){animate2((distance+step));}, tick);
  }
/**
 * -------------------------------Car Animation--------------------------------
 */
var updatedTimeout;
var update_time = 0;
function updated(){
  update_time++;
  document.getElementById('update_time').innerHTML = update_time + ' sec';
}


