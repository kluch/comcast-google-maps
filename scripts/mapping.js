/**
 * This is where the map is made and any modifications involving the map is made here
 */
var legs;

/**
 * -----------------------------Map Initialzation------------------------------
 */
function makeMap(mapObject, mapOptions, location, rendererOptions){

  map = new google.maps.Map(mapObject, mapOptions);

  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( {'address': location}, function(results, status) {
    goTo(results[0].geometry.location);
  });

  // create display service, not needed if I remove right panel
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
  // Instantiate a directions service.
  directionsService = new google.maps.DirectionsService();
}
/**
 * -----------------------------Map Initialzation------------------------------
 */

/**
 * --------------------------Map Panning and Zooming---------------------------
 */
function offsetCenter(latlng,offsetx,offsety) {

	// latlng is the apparent center-point
	// offsetx is the distance you want that point to move to the right, in pixels
	// offsety is the distance you want that point to move upwards, in pixels
	// offset can be negative
	// offsetx and offsety are both optional

	var scale = Math.pow(2, map.getZoom());
	var nw = new google.maps.LatLng(
	    map.getBounds().getNorthEast().lat(),
	    map.getBounds().getSouthWest().lng()
	);

	var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
	var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0);

	var worldCoordinateNewCenter = new google.maps.Point(
	    worldCoordinateCenter.x - pixelOffset.x,
	    worldCoordinateCenter.y + pixelOffset.y
	);

	var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

	map.setCenter(newCenter);

}

function goTo(latlng, firstrun){
  "use strict";
  var scale = Math.pow(2, map.getZoom());
  var x = mapWidth / 2;
  var offsetx = ((mapWidth - menuWidth) / 2) + menuWidth - x;
  
  var mapsCenter = map.getProjection().fromLatLngToPoint(latlng);
  var pixelOffset = new google.maps.Point((offsetx/scale) || 0, 0);

  // Changes latlng value to the new center
  var mapsNewCenter = new google.maps.Point(mapsCenter.x - pixelOffset.x, mapsCenter.y);
  latlng = map.getProjection().fromPointToLatLng(mapsNewCenter);
  if(firstrun){
    map.setCenter(latlng);
  }
  else{
    map.panTo(latlng);
  }
}
/**
 * --------------------------Map Panning and Zooming---------------------------
 */


/**
 * ------------------------------Get Directions--------------------------------
 */
function getDirections(request) {
  // Route the directions and pass the response to a
  // function to create markers for each step.
  directionsService.route(request, function(response, status) {
    // If everything returns OK
    if (status == google.maps.DirectionsStatus.OK){
      // Setup the directions
      directionsDisplay.setDirections(response);
      var bounds = new google.maps.LatLngBounds();
      
      startLocation = {latlng: null,
                     address: null};
      endLocation = {latlng: null,
                     address: null};

      // TODO: Maybe remove this, might want to save this though
      // For each route, display summary information.
      //var path = response.routes[0].overview_path;

      // For each leg (typically only one leg.  leg = directions between waypoints)
      legs = response.routes[0].legs;
      var txtTime = "";
      var time = 0;
      var hours = 0;
      var minutes = 0;
      var totDistance = 0;

      startLocation.latlng = legs[0].start_location;
      startLocation.address = legs[0].start_address;

      for (var i=0;i<legs.length;i++) {
        if(i === 0) {

	        // This is only needed if canvas isn't supported.
	        // Support for no canvas isn't really here.. I could implement it with no_rotate code
	        // but currently the program isn't tested in canvasless mode.
	        if (!supportsCanvas) {
	          marker = new createMarker2(legs[i].start_location);
	        }

        }
        totDistance += legs[i].distance.value;
        time += legs[i].duration.value;
        endLocation.latlng = legs[i].end_location;
        endLocation.address = legs[i].end_address;
        var steps = legs[i].steps;
        for (var j=0;j<steps.length;j++) {
          var nextSegment = steps[j].path;
          for (k=0;k<nextSegment.length;k++) {
            polyline.getPath().push(nextSegment[k]);
            poly2.getPath().push(nextSegment[k]);
            bounds.extend(nextSegment[k]);
          }
        }
      }
      currentETA = time;
      hours = Math.floor(time/3600);
      time = time - (hours*3600);
      minutes = Math.floor(time/60);
      time = time - (minutes*60);
      if(hours>0){
        txtTime = hours+" hours";
        if(minutes>0){
          txtTime = txtTime+", "+minutes+" minutes";
          if(time>0){
            txtTime = txtTime+", "+time+" seconds";
          }
        }
      }
      else if(minutes>0){
        txtTime = minutes+" minutes";
        if(time>0){
          txtTime = txtTime+", "+time+" seconds";
        }
      }
      else if(time>0){
        txtTime = time+" seconds";
      }
      document.getElementById('distance').innerHTML = (Math.round10(0.000621371*totDistance,-1))+" miles";
      document.getElementById('time').innerHTML = "ETA: "+legs[0].duration.text;
      document.getElementById('address_txt').innerHTML = endLocation.address;


      // This updates the step var in animate to reflect realtime
      step = (polyline.Distance()/currentETA)*(tick/1000);
      console.log("new step distance: "+step);

      polyline.setMap(map);
      poly2.setMap(map);
      endMarker = createMarker2(endLocation.latlng);
      directionsDisplay.setMap(map);
      directionsDisplay.setPanel(document.getElementById('directionsPanel'));
      map.fitBounds(bounds);
      goTo(bounds.getCenter(), true);
      //createMarker(endLocation.latlng,"end",endLocation.address,"red");
      //map.setZoom(7);
      startAnimation2();
    }
  });
}
/**
 * ------------------------------Get Directions--------------------------------
 */