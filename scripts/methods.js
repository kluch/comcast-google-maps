//   This Javascript was originally provided by Mike Williams
//   Blackpool Community Church Javascript Team
//   http://www.commchurch.freeserve.co.uk/
//   http://econym.org.uk/gmap/
//   Modified by Joe Kluchinski

// TODO: Clean up methods and figure out exactly what some of them do..
var timerHandle;
var marker;
var startLocation = {latlng: null,
                     address: null};
var endLocation = {latlng: null,
                     address: null};
var currentETA;

// Runs when start button is pressed
function start() {
  // resets values
  // this shouldn't be needed once I get rid of the start button
  if (timerHandle) { clearTimeout(timerHandle); }
  if (updateETATimeout) { clearInterval(updateETATimeout); }
  if (updatedTimeout) { clearInterval(updatedTimeout); }
  if (marker) { marker.setMap(null);}
  distance = 0;
  polyline.setMap(null);
  poly2.setMap(null);
  directionsDisplay.setMap(null);

  

  polyline = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3
  });

  // Create a renderer for directions and bind it to the map.
  var rendererOptions = {
    map: map
  };
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

  var startpoint = document.getElementById("startpoint").value;
  var endpoint = document.getElementById("endpoint").value;
  var travelMode = google.maps.DirectionsTravelMode.DRIVING;

  var request = {
    origin: startpoint,
    destination: endpoint,
    travelMode: travelMode
  };

  getDirections(request);
  // This should check if there is new live data, if there is
  // build a request with the latlng otherwise dec the ETA
  updateETATimeout = setInterval(updateETA, 60000);
  updatedTimeout = setInterval(updated, 1000);
}

function createMarker2(latlng1) {
  var latlng = latlng1;
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    clickable: false,
    visible: true,
    //icon: {url: 'xcomcast.jpg', scaledSize: {width:100, height:50}},
    //zIndex: Math.round(latlng.lat()*-100000)<<5
  });
  return marker;
}

var step = 5; // 5 metres
var tick = 100; // milliseconds
var distance = 0;
var eol;
var k=0;
var stepnum=0;
var speed = "";
var lastVertex = 1;


//=============== animation functions ======================
    function updatePoly(d) {
      // Spawn a new polyline every 20 vertices, because updating a 100-vertex poly is too slow
      console.log("i'm updating the poly!");
      if (poly2.getPath().getLength() > 20) {
        poly2=new google.maps.Polyline([polyline.getPath().getAt(lastVertex-1)]);
        // map.addOverlay(poly2)
      }

      if (polyline.GetIndexAtDistance(d) < lastVertex+2) {
        if (poly2.getPath().getLength()>1) {
          poly2.getPath().removeAt(poly2.getPath().getLength()-1);
        }
        poly2.getPath().insertAt(poly2.getPath().getLength(),polyline.GetPointAtDistance(d));
      }
      else {
        poly2.getPath().insertAt(poly2.getPath().getLength(),endLocation.latlng);
      }
    }


    


    function startAnimation() {
      eol=polyline.Distance();
      map.setCenter(polyline.getPath().getAt(0));

      poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(0)], strokeColor:"#0000FF", strokeWeight:10});
      window.setTimeout(function(){animate(0);},2000);  // Allow time for the initial map display
    }
    function startAnimation2() {
        eol=polyline.Distance();
        // map.setCenter(polyline.getPath().getAt(0));

        if (true/*supportsCanvas*/) {
          // Canvas size is set big enough so that the rotated image won't get cropped
          // The google maps size is half the canvas (-x,y) so that it is centered on the polyline
          marker = new ELabel(map,
            polyline.getPath().getAt(0),
            '<canvas id="carcanvas" width="142" height="142">'+
            'Your browser does not support the HTML5 &lt;canvas&gt; tag, please update your browser'+
            ' to make use of this feature.<\/canvas>',
            'the_car',
            new google.maps.Size(-71,71)
          );
          
          // Polysteps is used in ELabel3 and plotCar
          // TODO: See if I need polysteps.. I do not need it, I'm replacing it with distance.
          // Probably should rename distance, location is a better descriptor
          polysteps = 0;

          marker.setMap(map);

        }
        // This was actually done in start() and would only be used if browser doesn't
        // support the canvas tag
        /*
        else {
            marker = new google.maps.Marker({
              position: latlng,
              map: map,
              clickable: false,
              raiseOnDrag: false,
              flat: true,
              icon: {url: 'xcomcast.jpg', scaledSize: {width:100, height:50}},
              zIndex: Math.round(latlng.lat()*-100000)<<5
            });
          map.addOverlay(marker);
        }
        */

        // Allow time for the initial map display
        // animation happens after two seconds
        window.setTimeout(function(){animate2(distance);},2000);
      }
//=============== ~animation funcitons =====================
// Rounding Function
(function(){
  /**
   * Decimal adjustment of a number.
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number}      The adjusted value.
   */
  function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }

})();