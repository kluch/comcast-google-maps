/**
 * ---------------------------------Get Angle----------------------------------
 */
// Returns the bearing in radians between two points.
function bearing( from, to ) {

  // Get the x and y values for where we are and where we're going
  var lat1 = from.lat();
  var lng1 = from.lng();
  var lat2 = to.lat();
  var lng2 = to.lng();

  // Subtract to-from so we have values in relation to (0,0)
  // returns radians
  // (0,1) = 0;
  // (1,0) = PI / 2
  // (-1,0) = -(PI /2)
  // (0,-1) = PI
  var angle = Math.atan2((lng2-lng1),(lat2-lat1));
  
  // converts from radians to degrees
  angle *= 180 / Math.PI;
  
  // console.log('angle in degrees:');
  // console.log(angle* (180/Math.PI));
  // console.log('angle in radians:');
  // console.log(angle/Math.PI+'π');
  return angle;
}
/**
 * ---------------------------------Get Angle----------------------------------
 */
/**
 * ---------------------------------Plot Car-----------------------------------
 */
// canvas is a square
var canvasSize = 142;
var carDir = null;
var carCount = 0;

function plotcar2(location){
  var p0 = polyline.GetPointAtDistance(location);
  var p1 = polyline.GetPointAtDistance(location+step);
  var angle = bearing(p0,p1);

  canvas.clearRect(0,0,142,142);

  // If car is facing left
  if(carDir === 'left'){
    if(angle < 0 && angle > -180){
      console.log('The img is left facing image');
      drawCar(img, angle, canvasSize, 561, 0, 90, 244, 35, 90);
      carCount = 0;
    }
    else if(carCount < 7){
      console.log('The img should face right but too small a count');
      drawCar(img, angle, canvasSize, 561, 0, 90, 244, 35, 90);
      carCount++;
    }
    else{
      console.log('The img may now face right');
      drawCar(img, angle, canvasSize, 802, 0, 90, 244, 35, 90);
      carDir = 'right';
      carCount = 0;
    }
  }
  // If car is facing right
  else if(carDir === 'right'){
    if(!(angle < 0 && angle > -180)){
      console.log('The img is right facing image');
      drawCar(img, angle, canvasSize, 802, 0, 90, 244, 35, 90);
      carCount = 0;
    }
    else if(carCount < 7){
      console.log('The img should face left but too small a count');
      drawCar(img, angle, canvasSize, 802, 0, 90, 244, 35, 90);
      carCount++;
    }
    else{
      console.log('The img may now face left');
      drawCar(img, angle, canvasSize, 561, 0, 90, 244, 35, 90);
      carDir = 'left';
      carCount = 0;
    }
  }
  // If the car doesn't have a direction set yet
  else{
    if(angle < 0 && angle > -180){
      console.log('The img is left facing image');
      canvas.drawImage(img, 561, 0, 90, 244, 0+71-18, 0+71-45, 35, 90);
      $("#carcanvas").rotate({
        duration:1000, 
        animateTo:(angle),
      });
      carDir = 'left';
    }
    else{
      console.log('The img is right facing image');
      drawCar(img, angle, canvasSize, 802, 0, 90, 244, 35, 90);
      carDir = 'right';
    }
  }
}
/**
 * ---------------------------------Plot Car-----------------------------------
 */
/**
 * ---------------------------------Draw Car-----------------------------------
 */
// ix and iy are the coordinates of the top left of what we want in the canvas
// iw and iw are the width and height of the img section we want to use
// cw and ch are the size we want the image to be in the canvas
function drawCar(img, angle, canvSize, ix, iy, iw, ih, cw, ch){
  canvas.drawImage(img, ix, iy, iw, ih, (canvSize/2)-(cw/2), (canvSize/2)-(ch/2), cw, ch);
  $("#carcanvas").rotate({
    duration:1000, 
    animateTo:(angle)
  });
}
/**
 * ---------------------------------Draw Car-----------------------------------
 */

