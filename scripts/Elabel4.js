// ELabels3.js
//
//   2010-06-10  Port to GoogleMaps V3 API by Pat Horton
//
//   This Javascript was originally provided by Mike Williams
//   Blackpool Community Church Javascript Team
//   http://www.commchurch.freeserve.co.uk/
//   http://econym.googlepages.com/index.htm
//
//   This work is licenced under a Creative Commons Licence
//   http://creativecommons.org/licenses/by/2.0/uk/
//   
ELabel.prototype = new google.maps.OverlayView();

/**
 * CustomOverlayView
 * @param {Map}    map
 * @param {LatLng} latlng  Where the div should be on the map
 * @param {String} canvasName     Name of the CanvasElement
 * @param {Size}   pixelOffset   pixelOffset on x coordinates (+ = left)
 */
function ELabel(map, latlng, canvasName, image, pixelOffset, elementSize){
  // this.point = point;
  // this.html = html;
  // this.classname = classname || "";
  // this.pixelOffset = pixelOffset || new google.maps.Size(0,0);
  /* if (percentOpacity)
  {
    if (percentOpacity<0) percentOpacity=0;
    if (percentOpacity>100) percentOpacity=100;
  } */
  // this.percentOpacity = percentOpacity;
  // this.overlap=overlap || false;
  // this.hidden = false;

  // Initialize all properties.
  this.point_ = latlng;
  this.image_ = image;
  this.canvasName_ = canvasName;
  this.pixelOffset_ = pixelOffset || new google.maps.Size(0,0);
  this.elementSize_ = elementSize || new google.maps.Size(100,100);
  this.map_ = map;

  // Define a property to hold the image's div. We'll
  // actually create this div upon receipt of the onAdd()
  // method so we'll leave it null for now.
  this.div_ = null;

  // Explicitly call setMap on this overlay.
  this.setMap(map);
}

ELabel.prototype.onAdd = function()
{
  var div = document.createElement('div');
  div.style.borderStyle = 'none';
  div.style.borderWidth = '0px';
  div.style.position = 'absolute';

  // Create the canvas element and attach it to the div.
  var canvas = document.createElement('canvas');
  canvas.innerHTML = 'Your browser does not support the HTML5 &lt;canvas&gt; tag, '+
                     'please update your browser to make use of this feature.';
  canvas.style.width = '100%';
  canvas.style.height = '100%';
  canvas.style.position = 'absolute';
  canvas.id = canvasName;
  div.appendChild(canvas);

  // Create the img element and attach it to the canvas.
  var img = document.createElement('img');
  img.src = this.image_;
  img.style.width = '100%';
  img.style.height = '100%';
  img.style.position = 'absolute';
  canvas.appendChild(img);

  this.div_ = div;

  // Add the element to the "overlayLayer" pane.
  var panes = this.getPanes();
  panes.overlayLayer.appendChild(div);

};

ELabel.prototype.draw = function()
{
  console.log("draw");
  // var proj = this.getProjection();
  // // var p = proj.fromLatLngToDivPixel(this.point);
  // var h = parseInt(this.div_.clientHeight);
  // this.div_.style.left = (proj.x + this.pixelOffset.width) + "px";
  // this.div_.style.top = (proj.y +this.pixelOffset.height - h) + "px";

  // We use the polyline point for the correct position
  // We first need the projection from the overlay
  var overlayProjection = this.getProjection();
  
  // The pixel location we want the div to start at.
  var p = overlayProjection.fromLatLngToDivPixel(this.point_);

  var div = this.div_;
  div.style.left = (p.x + this.pixelOffset.width) + "px";
  div.style.top = (p.y - this.pixelOffset.height) + "px";
  div.style.width = (elementSize_.width) + 'px';
  div.style.height = (elementSize_.height) + 'px';

  // var h = parseInt(this.div_.clientHeight);
  // this.div_.style.left = (p.x + this.pixelOffset.width) + "px";
  // this.div_.style.top = (p.y +this.pixelOffset.height - h) + "px";
  //   if (this.overlap)
  //   {
  //     var z = GOverlay.getZIndex(this.point.lat());
  //     this.div_.style.zIndex = z;
  //   }
  //   this.redraw(true);
};

ELabel.prototype.onRemove = function()
{
  this.div_.parentNode.removeChild(this.div_);
  this.div_ = null;
};

ELabel.prototype.show = function()
{
  if (this.div_)
  {
    this.div_.style.display="";
    this.redraw();
  }
  this.hidden = false;
};

ELabel.prototype.hide = function()
{
  if (this.div_)
  {
    this.div_.style.display="none";
  }
  this.hidden = true;
};



ELabel.prototype.copy = function()
{
  return new ELabel(this.point, this.html, this.classname,
    this.pixelOffset, this.percentOpacity, this.overlap);
};

ELabel.prototype.redraw = function(force) {
  // var p = this.map_.fromLatLngToDivPixel(this.point);
  //
  //polysteps = this.point;
  // plotcar();
  
  // console.log("redraw");
  //polysteps++;
  //console.log(this.point);
  var p = this.getProjection().fromLatLngToDivPixel(this.point);
  var h = parseInt(this.div_.clientHeight);
  this.div_.style.left = (p.x + this.pixelOffset.width) + "px";
  this.div_.style.top = (p.y +this.pixelOffset.height - h) + "px";
};

ELabel.prototype.isHidden = function()
{
  return this.hidden;
};

ELabel.prototype.supportsHide = function()
{
  return true;
};

ELabel.prototype.setContents = function(html)
{
  this.html = html;
  this.div_.innerHTML = '<div class="' + this.classname + '">' +
  this.html + '</div>' ;
  this.redraw(true);
};

// ELabel.prototype.setPoint = function(point)
// {
//   this.point = point;
//   if (this.overlap)
//   {
//     var z = GOverlay.getZIndex(this.point.lat());
//     this.div_.style.zIndex = z;
//   }
//   this.redraw(true);
// };

ELabel.prototype.setOpacity = function(percentOpacity)
{
  if (percentOpacity)
  {
    if(percentOpacity<0){percentOpacity=0;}
    if(percentOpacity>100){percentOpacity=100;}
  }
  this.percentOpacity = percentOpacity;
  if (this.percentOpacity)
  {
    if(typeof(this.div_.style.filter)=='string')
      {this.div_.style.filter='alpha(opacity:'+this.percentOpacity+')';}
    if(typeof(this.div_.style.KHTMLOpacity)=='string')
      {this.div_.style.KHTMLOpacity=this.percentOpacity/100;}
    if(typeof(this.div_.style.MozOpacity)=='string')
      {this.div_.style.MozOpacity=this.percentOpacity/100;}
    if(typeof(this.div_.style.opacity)=='string')
      {this.div_.style.opacity=this.percentOpacity/100;}
  }
};

ELabel.prototype.getPoint = function()
{
  return this.point;
};