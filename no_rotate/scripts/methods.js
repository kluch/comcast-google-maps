//TODO: Clean up methods and figure out exactly what some of them do..
function calcRoute(){
      //resets values
      if (timerHandle) { clearTimeout(timerHandle); }
      if (marker) { marker.setMap(null);}
      polyline.setMap(null);
      poly2.setMap(null);
      directionsDisplay.setMap(null);


      polyline = new google.maps.Polyline({
        path: [],
        strokeColor: '#FF0000',
        strokeWeight: 3
      });
      //this one is invis for some reason
      poly2 = new google.maps.Polyline({
        path: [],
        strokeColor: '#FF0000',
        strokeWeight: 3
      });

      // Create a renderer for directions and bind it to the map.
      var rendererOptions = {
        map: map
      };
      directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

      // Gets the directions from the form
      var start = document.getElementById("start").value;
      var end = document.getElementById("end").value;
      var travelMode = google.maps.DirectionsTravelMode.DRIVING;

      var request = {
        origin: start,
        destination: end,
        travelMode: travelMode
      };

      // Route the directions and pass the response to a
      // function to create markers for each step.
      directionsService.route(request, function(response, status) {

        // If everything returns OK
        if (status == google.maps.DirectionsStatus.OK){
          // Setup the directions
          directionsDisplay.setDirections(response);
          var bounds = new google.maps.LatLngBounds();
          var route = response.routes[0];
          startLocation = {};
          //this line and the one below are equiv
          endLocation = {};
          // endLocation = new Object();

          // For each route, display summary information.
          var path = response.routes[0].overview_path;
          var legs = response.routes[0].legs;
          console.log("number of legs: "+legs.length);
          // For each leg (typically only one leg.  leg = directions between waypoints)
          for (i=0;i<legs.length;i++) {
            if(i === 0) {
              startLocation.latlng = legs[i].start_location;
              startLocation.address = legs[i].start_address;
              // marker = google.maps.Marker({map:map,position: startLocation.latlng});
              // marker = new google.maps.Marker({position:myCenter,icon:'pinkball.png'});
              marker = new createMarker2(legs[i].start_location);
              // console.log(marker);
              // marker = createMarker(legs[i].start_location,"Start",legs[i].start_address,"green");
            }
            endLocation.latlng = legs[i].end_location;
            endLocation.address = legs[i].end_address;
            var steps = legs[i].steps;

            for (j=0;j<steps.length;j++) {
              var nextSegment = steps[j].path;
              for (k=0;k<nextSegment.length;k++) {
                polyline.getPath().push(nextSegment[k]);
                bounds.extend(nextSegment[k]);
              }
            }
          }

          polyline.setMap(map);
          map.fitBounds(bounds);
          //createMarker(endLocation.latlng,"end",endLocation.address,"red");
          map.setZoom(18);
          startAnimation();
        }
      });
    }

    function createMarker2(latlng1) {
      // alert("createMarker("+latlng+","+label+","+html+","+color+")");
      /*var contentString = '<b>'+label+'</b><br>'+html;
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: label,
        zIndex: Math.round(latlng.lat()*-100000)<<5
      });
      marker.myname = label;
      // gmarkers.push(marker);
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
      });*/
      latlng = latlng1;
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        clickable: false,
        raiseOnDrag: false,
        flat: true,
        icon: {url: 'xcomcast.jpg', scaledSize: {width:100, height:50}},
        zIndex: Math.round(latlng.lat()*-100000)<<5
      });
      return marker;
    }

    function createMarker(latlng, label, html) {
      // alert("createMarker("+latlng+","+label+","+html+","+color+")");
      var contentString = '<b>'+label+'</b><br>'+html;
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: label,
        zIndex: Math.round(latlng.lat()*-100000)<<5
      });
      marker.myname = label;
      // gmarkers.push(marker);
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
      });
      return marker;
    }

    // Returns the bearing in radians between two points.
    function bearing( from, to ) {
    // Convert to radians.
    // console.log(from);
    // var lat1 = from.latRadians();
    // var lon1 = from.lngRadians();
    // var lat2 = to.latRadians();
    // var lon2 = to.lngRadians();
    // // Compute the angle.
    // var angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );
    // if ( angle < 0.0 )
    //   angle  += Math.PI * 2.0;
    // if (angle === 0) {angle=1.5;}
    // return angle;
    
    //triangle method

      //I don't think I want/need Radians here.. but maybe I do
      var lat1 = from.lat();
      var lng1 = from.lng();
      var lat2 = to.lat();
      var lng2 = to.lng();

      angle = Math.atan2((lng2-lng1),(lat2-lat1));
      angle -= Math.PI / 2;
      
      /*if(angle > Math.PI){
        angle  -= Math.PI * 2.0;
      }
      angle = Math.PI + (angle % Math.PI);*/
      //angle = Math.atan2((0),(-1));
      console.log('angle');
      console.log(angle/Math.PI);
      return angle;

      //triangle method

    }

    var canvasWidth = 142, canvasHeight = 142;
    function plotcar() {
      //canvas = document.getElementById("carcanvas").getContext('2d');
      console.log("polysteps below");
      console.log(polysteps);
      console.log("polysteps+steps below");
      console.log(polyline.getPath());
      console.log("this is the eol: "+eol);
      /*var p0 = polyline.getPath().getAt(polysteps);
      var p1 = polyline.getPath().getAt(polysteps+step);*/
      var p0 = polyline.GetPointAtDistance(polysteps);
      var p1 = polyline.GetPointAtDistance(polysteps+step);
      polysteps+=step;
      angle = bearing(p0,p1);

      var cosa = Math.cos(angle);
      var sina = Math.sin(angle);
      canvas.clearRect(0,0,142,142);
      canvas.save();
      //canvas.rotate(angle);
      // canvas.translate(32*sina+32*cosa,32*cosa-32*sina);
      // canvas.drawImage(rImg,-16,-16);
      console.log('The img width is '+rImg.width);
      canvas.translate(canvasWidth/2, canvasHeight/2);
      canvas.rotate(angle);
      canvas.translate(-canvasWidth/2, -canvasHeight/2);
      canvas.drawImage(rImg,canvasWidth/2-rImg.width/2, canvasHeight/2-rImg.height/2-21);
      /*if(angle > (Math.PI/2)){
        // flip context horizontally
        canvas.scale(-1, 1);
      }*/
      canvas.restore();
    }

    var step = 5; // 5; // metres
    var tick = 100; // milliseconds
    var eol;
    var k=0;
    var stepnum=0;
    var speed = "";
    var lastVertex = 1;


//=============== animation functions ======================
    function updatePoly(d) {
      // Spawn a new polyline every 20 vertices, because updating a 100-vertex poly is too slow
      console.log("i'm updating the poly!");
      if (poly2.getPath().getLength() > 20) {
        poly2=new google.maps.Polyline([polyline.getPath().getAt(lastVertex-1)]);
        // map.addOverlay(poly2)
      }

      if (polyline.GetIndexAtDistance(d) < lastVertex+2) {
        if (poly2.getPath().getLength()>1) {
          poly2.getPath().removeAt(poly2.getPath().getLength()-1);
        }
        poly2.getPath().insertAt(poly2.getPath().getLength(),polyline.GetPointAtDistance(d));
      }
      else {
        poly2.getPath().insertAt(poly2.getPath().getLength(),endLocation.latlng);
      }
    }


    function animate(d) {
      // alert("animate("+d+")");
      if (d>eol) {
        map.panTo(endLocation.latlng);
        marker.setPosition(endLocation.latlng);
        return;
      }
      var p = polyline.GetPointAtDistance(d);
      map.panTo(p);
      marker.setPosition(p);
      updatePoly(d);
      // setTimeout(animate((d+step)), tick) doesn't work,
      // setTimeout("animate("+(d+step)+")", tick) works but is very bad
      // Putting animate within an anonymous function works
      // http://javascriptweblog.wordpress.com/2010/04/19/how-evil-is-eval/
      timerHandle = window.setTimeout(function(){animate((d+step));}, tick);
    }


    function startAnimation() {
      eol=polyline.Distance();
      map.setCenter(polyline.getPath().getAt(0));

      poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(0)], strokeColor:"#0000FF", strokeWeight:10});
      window.setTimeout(function(){animate(0);},2000);  // Allow time for the initial map display
    }
//=============== ~animation funcitons =====================