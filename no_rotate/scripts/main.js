//=============== Required for api ===============

//=============== Required for api ===============
//TODO: Clean up variables
var map;
var directionDisplay;
var directionsService;
var stepDisplay;
var dirn;
var markerArray = [];
var position;
var polysteps;
var latlng;
var k=0; //this is var is for canvas test
var marker = null;
var polyline = null;
var poly2 = null;
var speed = 0.000005, wait = 1;
var infowindow = null;
var supportsCanvas = null;
var myPano;   
var panoClient;
var nextPanoId;
var timerHandle = null;
var rImg = new Image();
var steps = [];
var canvas;


// Runs onload of <body>
function initialize() {
  // Instantiate a directions service.
  directionsService = new google.maps.DirectionsService();

  // This checks for the testcanvas (which should exists)
  if (document.getElementById('testcanvas').getContext) {
    supportsCanvas = true;
  }
  else {
    supportsCanvas = false;
  }
  rImg.src='xcomcast_small.jpg';
  // rImg.width='100';
  // rImg.height='46';
  
  // Map options with UI disabled.
  var myOptions = {
    zoom: 7,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  };

  // Create map centered on new york
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  address = 'new york';
  geocoder = new google.maps.Geocoder();
  geocoder.geocode( {'address': address}, function(results, status) {
    map.setCenter(results[0].geometry.location);
  });

  // Create a renderer for directions and bind it to the map.
  var rendererOptions = {
    map: map
  };
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
  
  // Instantiate an info window to hold step text.
  // stepDisplay = new google.maps.InfoWindow();

  // These lines get created here but I believe both get overwritten almost immediately..
  // Only reason to leave them in is so setting them to null in calcRoute doesn't throw an error
  polyline = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3
  });
  poly2 = new google.maps.Polyline({
    path: [],
    strokeColor: '#FF0000',
    strokeWeight: 3
  });

  //This happens on click
  //calcRoute();

}